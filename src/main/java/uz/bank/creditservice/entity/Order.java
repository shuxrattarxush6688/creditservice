package uz.bank.creditservice.entity;

import lombok.Data;
import uz.bank.creditservice.enums.Status;

import javax.persistence.*;

@Data
@Entity(name = "Oders")
public class Order { //order for getting credit

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String passportSeries;

    private String passportNumber;

    private String name;

    private String surname;

    private String birthDate;

    private Long salary;

    private Long amountCredit;

    @Enumerated(EnumType.STRING)
    private Status status = Status.NEW;
}
