package uz.bank.creditservice.enums;

public enum Status {
    NEW, ACCEPTED, REJECTED
}
