package uz.bank.creditservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CitizenDto {

    private String passportSeries;

    private String passportNumber;

    private String name;

    private String surname;

    private String birthDate;

}
