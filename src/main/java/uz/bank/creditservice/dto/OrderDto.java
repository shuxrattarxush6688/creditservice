package uz.bank.creditservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.bank.creditservice.entity.Order;
import uz.bank.creditservice.enums.Status;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private Long id;

    private String passportSeries;

    private String passportNumber;

    private Long salary;

    private Long creditAmount;

    private Status status;

    public static OrderDto toDto(Order order){
        OrderDto dto = new OrderDto();
        dto.setCreditAmount(order.getAmountCredit());
        dto.setPassportNumber(order.getPassportNumber());
        dto.setSalary(order.getSalary());
        dto.setPassportSeries(order.getPassportSeries());
        dto.setId(order.getId());
        dto.setStatus(order.getStatus());
        return dto;
    }

}
