package uz.bank.creditservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.bank.creditservice.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
