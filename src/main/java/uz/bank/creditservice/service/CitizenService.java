package uz.bank.creditservice.service;

import uz.bank.creditservice.dto.CitizenDto;

public interface CitizenService {

    public CitizenDto find(String serial, String number);
}
