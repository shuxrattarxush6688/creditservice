package uz.bank.creditservice.service;

import uz.bank.creditservice.dto.OrderDto;

import java.util.List;

public interface CreditService {

    public void addOrder(OrderDto dto);
    public void calculateOrder(Long id);
    public List<OrderDto> findAll();


}
