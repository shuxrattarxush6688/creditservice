package uz.bank.creditservice.service.impl;

import org.springframework.stereotype.Service;
import uz.bank.creditservice.dto.CitizenDto;
import uz.bank.creditservice.dto.OrderDto;
import uz.bank.creditservice.entity.Order;
import uz.bank.creditservice.enums.Status;
import uz.bank.creditservice.repository.OrderRepository;
import uz.bank.creditservice.service.CitizenService;
import uz.bank.creditservice.service.CreditService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CreditServiceImpl implements CreditService {

    private final CitizenService citizenService;
    private final OrderRepository repository;


    public CreditServiceImpl(CitizenService citizenService, OrderRepository repository) {
        this.citizenService = citizenService;
        this.repository = repository;
    }

    @Override
    public void addOrder(OrderDto dto) {
        Order order = new Order();
        CitizenDto citizen = citizenService.find(dto.getPassportSeries(), dto.getPassportNumber());

        if (citizen == null) {
            System.out.println("Fuqaro topilmadi");
            return; //todo exception yozish kerak
        }

        order.setName(citizen.getName());
        order.setSurname(citizen.getSurname());
        order.setBirthDate(citizen.getBirthDate());
        order.setSalary(dto.getSalary());
        order.setAmountCredit(dto.getCreditAmount());
        order.setPassportNumber(citizen.getPassportNumber());
        order.setPassportSeries(citizen.getPassportSeries());
        repository.save(order);
    }

    @Override
    public void calculateOrder(Long id) {
        Optional<Order> optionalOrder = repository.findById(id);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();

            if (order.getStatus() != Status.NEW) {
                System.out.println("Order already calculated");
            }

            double salary = order.getSalary();
            double amount = order.getAmountCredit();
            double percentage = 20;
            double period = 3 * 12;
            double result;
            result = (amount / period) + amount * percentage / (12 * 100);

            if (salary >= result) {
                System.out.println("Arizangiz qabul qilindi.");
                order.setStatus(Status.ACCEPTED);
                repository.save(order);
            } else {
                System.out.println("Arizangiz qabul qilinmadi!");
                order.setStatus(Status.REJECTED);
                repository.save(order);
            }
        } else {
            System.out.println("Order topilmadi ID: " + id);
        }
    }

    @Override
    public List<OrderDto> findAll() {
        return repository.findAll().stream().map(OrderDto::toDto).collect(Collectors.toList());
    }
}

