package uz.bank.creditservice.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.bank.creditservice.dto.CitizenDto;
import uz.bank.creditservice.entity.Order;
import uz.bank.creditservice.repository.OrderRepository;
import uz.bank.creditservice.service.CitizenService;

@Service
public class CitizenServiceImpl implements CitizenService {

    private final OrderRepository repository;

    private final String URI = "http://localhost:8090/api/v1/citizen";

    public CitizenServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public CitizenDto find(String serial, String number) {
        RestTemplate restTemplate = new RestTemplate();
        CitizenDto citizenDto = restTemplate.getForObject(URI + "?number=" + number + "&serial=" + serial, CitizenDto.class);
        return citizenDto;
    }
}
