package uz.bank.creditservice.controller;

import org.springframework.web.bind.annotation.*;
import uz.bank.creditservice.dto.OrderDto;
import uz.bank.creditservice.service.CreditService;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class BankController {
    private final CreditService service;

    public BankController(CreditService service) {
        this.service = service;
    }


    @PostMapping
    public void add(@RequestBody OrderDto dto) {
        service.addOrder(dto);
    }

    @PutMapping("{id}")
    public String calc(@PathVariable Long id){
        service.calculateOrder(id);
        return "Amal bajarildi";
    }

    @GetMapping
    public List<OrderDto> findAll(){
       return service.findAll();
    }
}

